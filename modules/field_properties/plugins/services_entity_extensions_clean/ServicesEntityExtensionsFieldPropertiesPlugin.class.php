<?php

class ServicesEntityExtensionsFieldPropertiesPlugin {

  /**
   * @var ServicesEntityResourceControllerClean
   */
  protected $controller;

  function __construct($plugin, $controller) {
    $this->controller = $controller;
  }

  public function get_data(&$result, $wrapper, $fields) {


    if ($fields != '*') {
      $fields_array = explode(',', $fields);
    }

    if ($wrapper instanceof EntityDrupalWrapper) {

      $wrapper_properties = $wrapper->getPropertyInfo();

      foreach($wrapper_properties as $field_name => $wrapper_property){
        $name = preg_replace('/^field_/', '', $field_name);
        if (!empty($wrapper_property['type'])
          && ($wrapper_property['type'] === 'field_properties'
            || entity_property_list_extract_type($wrapper_property['type']) === 'field_properties')
          && (empty($fields_array) || in_array($name, $fields_array))
        ) {


          $values = $wrapper->{$field_name}->value();

          if(!is_array($values)){
            $values = array($values);
          }

          $pretty_properties = array();
          foreach ($values as $property) {
            if ($property['type'] == 'json') {
              $property['value'] = json_decode($property['value']);
            }
            $pretty_properties[$property['name']] = $property['value'];
          }
          $result[$name] = $pretty_properties;

        }
      }




    }


  }


}
