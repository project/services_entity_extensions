<?php

class ServicesEntityExtensionsWebformNodePlugin {

  /**
   * @var ServicesEntityResourceControllerClean
   */
  protected $controller;

  function __construct($plugin, $controller) {
    $this->controller = $controller;
  }

  public function get_data(&$result, $wrapper, $fields) {

    switch ($wrapper->type()) {
      case 'file':
        $result['uri'] = $wrapper->value()->uri;
        break;
    }


    if ($wrapper->type() == 'node' && variable_get('webform_node_' . $wrapper->getBundle(), FALSE)) {
      $result['webform']['components'] = array();
      foreach ($wrapper->value()->webform['components'] as $component) {
        if (module_exists('webform_localization')) {
          module_load_include('inc', 'webform_localization', 'includes/webform_localization.i18n');
          $element = array('#title' => '');
          _webform_localization_translate_component($element, $component);
          if (!empty($element['#title'])) {
            $component['name'] = $element['#title'];
          }
        }
        $result['webform']['components'][] = $component;
      }
    }

  }


}
