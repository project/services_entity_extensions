<?php

class ServicesEntityExtensionsEntityReferenceLabelPlugin {

  /**
   * @var ServicesEntityResourceControllerClean
   */
  protected $controller;

  function __construct($plugin, $controller) {
    $this->controller = $controller;
  }

  public function get_resource_reference(&$result, $resource, $id) {
    $entity = entity_load_single($resource, $id);
    if($entity) {
      $result['label'] = entity_label($resource, $entity);
    }
  }


}
