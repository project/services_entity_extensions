<?php

class ServicesEntityExtensionsResourceControllerClean extends ServicesEntityResourceControllerClean {

  public function access($op, $args) {

    $result = parent::access($op, $args);

    $plugins = $this->getPlugins();
    foreach($plugins as $plugin){
      if(method_exists($plugin, 'access')){
        $plugin->access($result, $op, $args);
      }
    }

    return $result;

  }


  protected function getPlugins() {

    $plugin_instances = array();

    // Get all available plugins
    $plugins = ctools_get_plugins('services_entity_extensions', 'services_entity_extensions_clean');
    foreach ($plugins as $plugin) {
      $class_name = ctools_plugin_get_class($plugin, 'handler');
      $plugin_instances[] = new $class_name($plugin, $this);
    }

    return $plugin_instances;

  }

  protected function get_resource_reference($resource, $id) {
    $result = parent::get_resource_reference($resource, $id);
    $plugins = $this->getPlugins();
    foreach($plugins as $plugin){
      if(method_exists($plugin, 'get_resource_reference')){
        $plugin->get_resource_reference($result, $resource, $id);
      }
    }
    return $result;
  }

  protected function get_data($wrapper, $fields = '*') {
    $result = parent::get_data($wrapper, $fields);
    $plugins = $this->getPlugins();
    foreach($plugins as $plugin){
      if(method_exists($plugin, 'get_data')){
        $plugin->get_data($result, $wrapper, $fields);
      }
    }
    return $result;
  }

  public function create($entity_type, array $values) {
    $transaction = db_transaction();
    try {
      $plugins = $this->getPlugins();
      foreach ($plugins as $plugin) {
        if (method_exists($plugin, 'createBefore')) {
          $plugin->createBefore($entity_type, $values);
        }
      }
      $values = parent::create($entity_type, $values);
    } catch (Exception $e) {
      $transaction->rollback();
      throw $e;
    }
    return $values;
  }

  public function update($entity_type, $entity_id, array $values) {
    $transaction = db_transaction();
    try {
      $plugins = $this->getPlugins();
      foreach($plugins as $plugin){
        if(method_exists($plugin, 'updateBefore')){
          $plugin->updateBefore($entity_type, $entity_id, $values);
        }
      }
      $values =  parent::update($entity_type, $entity_id, $values);
    } catch (Exception $e) {
      $transaction->rollback();
      throw $e;
    }
    return $values;
  }

}
